# -*- coding: utf-8 -*-
import Globals, pygame
from Global_Funcs import save_settings, switch_music, switch_sounds
from random import randrange
from TransparentText import AlphaText
from Sprite import *

class MenuItem():
	def __init__(self, text, font, color, init_pos, always_visible, type, size, number, tooltip, alternative_init_pos=(0, 0), border_size=1, border_color='grey31', offset_axis='y', underline=False, force_border=False):
		self.x = init_pos[0]
		self.y = init_pos[1]
		self.AIP = alternative_init_pos
		self.border_size = border_size
		self.border_color = border_color
		if type in ('end_turn', 'manage_company', 'roll_dices'):
			self.obj = AlphaText(text, init_pos[0], init_pos[1], font, color)
		else:
			self.font = font
			if font:
				self.text = text
				self.color = color
			else:
				self.image = Sprite(init_pos[0], init_pos[1], Globals.DIRS['symbols'] + text)
		self.AV = always_visible
		self.type = type
		self.init_for_type()
		self.make_hotkeys()
		self.size = size
		self.offset_axis = offset_axis
		self.underline=underline
		self.force_border = force_border
		# Rect object to consider if that menuitem was selected by a mouse click
		self.active_zone = pygame.Rect((self.x-1+self.AIP[0], self.y+self.AIP[1]), (size[0]+1, size[1]))
		self.number = number
		self.tooltip = tooltip
	def init_for_type(self):
		if self.type == 'music_switch':
			self.text, self.color = self.music_and_sounds_symbols(Globals.SETTINGS['music'])
		elif self.type == 'sounds_switch':
			self.text, self.color = self.music_and_sounds_symbols(Globals.SETTINGS['sound_effects'])
	def make_hotkeys(self):
		if self.type in ('end_turn', 'roll_dices'):
			self.keys = (pygame.K_RETURN, pygame.K_KP_ENTER)
		else:
			self.keys = ()
	def music_and_sounds_symbols(self, current):
		if current == 18:
			return u'✔', Globals.COLORS['green']
		else:
			return u'✖', Globals.COLORS['red']
	def render(self, offset, mp):
		if self.offset_axis == 'y':
			self.active_zone = pygame.Rect((self.x-1+self.AIP[0], self.y+self.AIP[1]+offset), (self.size[0]+1, self.size[1]))
		else:
			self.active_zone = pygame.Rect((self.x-1+self.AIP[0]+offset, self.y+self.AIP[1]), (self.size[0]+1, self.size[1]))
		if self.active_zone.collidepoint(mp):
			if self.tooltip:
				self.tooltip.render(offset)
				if self.force_border:
					pygame.draw.rect(Globals.screen, Globals.COLORS[self.border_color], self.active_zone, self.border_size)
			elif self.underline:
				pygame.draw.line(Globals.screen, Globals.COLORS[self.border_color], self.active_zone.bottomleft, self.active_zone.bottomright, self.border_size)
			else:
				pygame.draw.rect(Globals.screen, Globals.COLORS[self.border_color], self.active_zone, self.border_size)
		if self.type == 'volume_level':
			if self.number+1 > Globals.SETTINGS['volume']*10:
				self.color = Globals.COLORS['grey63']
			else:
				self.color = Globals.COLORS['white']
		if self.type in ('end_turn', 'manage_company', 'roll_dices'):
			self.obj.y = self.y + offset
			self.obj.render(Globals.game_scr.tabs['main'].user_menu['alpha'])
		elif self.font:
			if self.offset_axis == 'y':
				pos = (self.x, self.y+offset)
			else:
				pos = (self.x+offset, self.y)
			Globals.screen.blit(self.font.render(self.text, True, self.color), pos)
		else:
			self.image.y = self.y + offset
			self.image.render()
	def action(self):
		if self.type == 'end_turn':
			Globals.game_scr.tabs['main'].end_turn()
		elif self.type == 'exit_main':
			if Globals.SETTINGS['music'] == 18:
				pygame.mixer.music.fadeout(2000)
			pygame.mixer.music.load(Globals.SOUNDS['menu_music'])
			if Globals.SETTINGS['music'] == 18:
				pygame.mixer.music.play(-1)
			Globals.main_menu_scr.init()
		elif self.type == 'music_switch':
			Globals.SETTINGS['music'] = switch_music(Globals.SETTINGS['music'])
			self.text, self.color = self.music_and_sounds_symbols(Globals.SETTINGS['music'])
		elif self.type == 'pause_game':
			Globals.game_scr.tabs['main'].pause_game()
		elif self.type == 'roll_dices':
			Globals.game_scr.tabs['main'].make_move()
		elif self.type == 'sounds_switch':
			Globals.SETTINGS['sound_effects'] = switch_sounds(Globals.SETTINGS['sound_effects'])
			self.text, self.color = self.music_and_sounds_symbols(Globals.SETTINGS['sound_effects'])
		elif self.type == 'volume_level':
			new = float(self.number+1)/10
			pygame.mixer.music.set_volume(new)
			Globals.SOUNDS['click'].set_volume(new)
			Globals.SETTINGS['volume'] = new
		if self.type in ('volume_level', 'music_switch', 'sounds_effects'):
			save_settings(Globals.SETTINGS)
class Tooltip():
	def __init__(self, init_pos, color, type='simple', text=None, number=None, align=None):
		self.color = color
		self.type = type
		if type == 'simple':
			self.text = text
			if align == 'right':
				self.x = Globals.menu_size[0] - Globals.FONTS['small'].size(text)[0] - init_pos[0]
			else:
				self.x = init_pos[0]
			self.y = init_pos[1]
		else:
			self.monobacs = ''
			if Globals.game_start_animation_scr.game == 'monopoly':
				self.monobacs += 'M'
			temp = init_pos[1]+15*len(Globals.FIELDS[str(number)]['rent'])
			self.number = number
			self.data = {'name'		: {'text'	: Globals.FIELDNAMES[str(number)],
									   'font'	: Globals.FONTS['small'],
									   'pos'	: init_pos},
						 'price'	: {'text'	: Globals.TRANSLATION[42]+Globals.FIELDS[str(number)]['price']['count']+self.monobacs,
									   'font'	: Globals.FONTS['micro'],
									   'pos'	: (init_pos[0], init_pos[1]+20)},
						 'rent'		: {'count'	: Globals.FIELDS[str(number)]['rent'],
						 			   'font'	: Globals.FONTS['micro'],
						 			   'pos'	: (init_pos[0], init_pos[1]+50)},
						 'pledge'	: {'text'	: Globals.TRANSLATION[43]+str(int(Globals.FIELDS[str(number)]['price']['count'])/2)+self.monobacs,
						 			   'font'	: Globals.FONTS['micro'],
						 			   'pos'	: (init_pos[0], temp+65)},
						 'rebuy'	: {'text'	: Globals.TRANSLATION[44]+str(int(int(Globals.FIELDS[str(number)]['price']['count'])/2*1.1))+self.monobacs,
						 			   'font'	: Globals.FONTS['micro'],
						 			   'pos'	: (init_pos[0], temp+80)}}
			self.current = 'price'
	def render(self, offset):
		if self.type == 'simple':
			Globals.screen.blit(Globals.FONTS['small'].render(self.text, True, self.color), (self.x, self.y+offset))
		else:
			for i in self.data.keys():
				if i == self.current:
					color = Globals.COLORS['light_green']
				else:
					color = self.color
				if i == 'rent':
					bacs = ''
					if Globals.FIELDS[str(self.number)]['group']['count'] == '9':
						bacs = 'M'
						temp = (6, 10)
					elif Globals.FIELDS[str(self.number)]['group']['count'] == '10':
						temp = (6, 8)
					else:
						if Globals.game_start_animation_scr.game == 'monopoly':
							bacs = 'M'
						temp = (0, len(self.data[i]['count']))
					for price in range(temp[0], temp[1]):
						Globals.screen.blit(self.data[i]['font'].render(Globals.RENTLABELS[price], True, color), (self.data[i]['pos'][0], self.data[i]['pos'][1]+15*(price-temp[0])))
						Globals.screen.blit(self.data[i]['font'].render(str(self.data[i]['count'][price-temp[0]])+bacs, True, color), (self.data[i]['pos'][0]+150-self.data[i]['font'].size(str(self.data[i]['count'][price-temp[0]]))[0], self.data[i]['pos'][1]+15*(price-temp[0])))
				else:
					Globals.screen.blit(self.data[i]['font'].render(self.data[i]['text'], True, color), self.data[i]['pos'])
