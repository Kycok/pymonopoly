# -*- coding: utf-8 -*-
import Globals, random, sys
from Menus import *
from Global_Funcs import *

#--- Template
class MenuScreen():
	def render(self, alpha):
		Globals.screen.fill(Globals.COLORS['white'])
		Globals.PICS['logo'].render()
		Globals.PICS['left_man'].render()
		Globals.PICS['right_man'].render()
		for string in self.static_text:
			string.render(alpha)
	def item_selected(self, item):
		self.menu.separate_alphas = {item	: 0}
		if Globals.SETTINGS['sound_effects'] == 18:
			Globals.SOUNDS['click'].play()
	def switch_game(self, game):
		if game == 6 and not Globals.SETTINGS['game_block']:
			return 5
		else:
			return 6

#--- Menu screens
class MainMenuScreen(MenuScreen):
	def __init__(self, font, itemsize):
		self.menu = VerticalMenu(self, font, itemsize)
		self.font = font
	def make_items(self):
		return ((AlphaText(Globals.TRANSLATION[0], 'center', 350, self.font, Globals.COLORS['black']), 0),
				(AlphaText(Globals.TRANSLATION[1], 'center', 400, self.font, Globals.COLORS['black']), 1),
				(AlphaText(Globals.TRANSLATION[2], 'center', 450, self.font, Globals.COLORS['black']), 2),
				(AlphaText(Globals.TRANSLATION[3], 'center', 500, self.font, Globals.COLORS['black']), 3))
	def init(self):
		self.static_text = (AlphaText(Globals.TRANSLATION[4] + Globals.game_version, 'center', 240, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText('Anthony Samartsev & Michael Mozhaev, 2014-2015', 'right', Globals.menu_size[1]-30, Globals.FONTS['small'], Globals.COLORS['black'], 12))
		self.menu.init()
	def item_selected(self, item):
		MenuScreen.item_selected(self, item)
		if item == 0:
			Globals.new_game_settings_menu_scr_1.init()
		if item == 1:
			Globals.prefs_menu_scr.init()
		elif item == 2:
			Globals.stats_menu_scr.init()
		elif item == 3:
			self.exit()
	def exit(self):
		sys.exit()

class StatsMenuScreen(MenuScreen):
	def __init__(self, font, itemsize):
		self.menu = VerticalMenu(self, font, itemsize)
		self.font = font
	def make_items(self):
		return ((AlphaText(self.game, 'center', 250, self.font, Globals.COLORS['black']), 0),
				(AlphaText(Globals.TRANSLATION[12], 'center', Globals.menu_size[1]-50, self.font, Globals.COLORS['black']), 1))
	def init(self):
		self.monopoly_stats, self.manager_stats = read_stats()
		if Globals.SETTINGS['favourite_game'] == 5 and not Globals.SETTINGS['game_block']:
			self.show_monopoly()
		else:
			self.show_manager()
		self.menu.init()
	def render(self, alpha):
		temp = self.stats[2]
		if int(self.stats[1]) and int(temp):
			temp += ' ('+str(round(int(temp)/float(self.stats[1])*100, 2))+' %)'
		offset = 0
		if Globals.SETTINGS['language']:
			offset += 50
		self.static_text = [AlphaText(Globals.TRANSLATION[9]+self.stats[1], 500-offset, 305, Globals.FONTS['medium'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[10]+temp, 500-offset, 335, Globals.FONTS['medium'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[11]+self.stats[3]+' M', 500-offset, 365, Globals.FONTS['medium'], Globals.COLORS['black'])]
		if not Globals.SETTINGS['game_block']:
			self.static_text.append(AlphaText(Globals.TRANSLATION[7], 'center', 275, Globals.FONTS['small'], Globals.COLORS['grey31']))
		for i in range(4, len(self.stats)):
			if int(self.stats[i]['score']):
				self.static_text.append(AlphaText(str(i-3)+'. '+self.stats[i]['name'], 525-offset, 365+20*i, Globals.FONTS['small'], Globals.COLORS['black']))
				self.static_text.append(AlphaText('  '*(10-len(self.stats[i]['score']))+self.stats[i]['score'], 625-offset, 365+20*i, Globals.FONTS['small'], Globals.COLORS['black']))
				self.static_text.append(AlphaText(self.stats[i]['date'], 775-offset, 365+20*i, Globals.FONTS['small'], Globals.COLORS[self.stats[i]['color']]))
		if len(self.static_text) == 4:
			self.static_text.append(AlphaText(Globals.TRANSLATION[40], 'center', 415, Globals.FONTS['small'], Globals.COLORS['red']))
		else:
			self.static_text.append(AlphaText(Globals.TRANSLATION[8], 500-offset, 410, Globals.FONTS['medium'], Globals.COLORS['black']))
		MenuScreen.render(self, alpha)
		pygame.draw.line(Globals.screen, Globals.COLORS['black'], (Globals.hor_center-150-offset, 400), (Globals.hor_center+150+offset, 400), 2)
	def item_selected(self, item):
		MenuScreen.item_selected(self, item)
		if item == 0:
			self.switch_stats()
		elif item == 1:
			self.exit()
	def switch_stats(self):
		if not Globals.SETTINGS['game_block']:
			if self.stats[0] == '0':
				self.show_manager()
			else:
				self.show_monopoly()
	def show_monopoly(self):
		self.stats = self.monopoly_stats
		self.game = Globals.TRANSLATION[5]
	def show_manager(self):
		self.stats = self.manager_stats
		self.game = Globals.TRANSLATION[6]
	def exit(self):
		Globals.main_menu_scr.init()

class PrefsMenuScreen(MenuScreen):
	def __init__(self, font, itemsize):
		self.menu = CombinedMenu(self, font, itemsize)
		self.static_text = self.make_static_text()
		self.font = font
	def make_static_text(self):
		return (AlphaText(Globals.TRANSLATION[13], 520, 290, Globals.FONTS['small'], Globals.COLORS['black']),
				AlphaText(Globals.TRANSLATION[14], 520, 340, Globals.FONTS['small'], Globals.COLORS['black']),
				AlphaText(Globals.TRANSLATION[15], 520, 390, Globals.FONTS['small'], Globals.COLORS['black']),
				AlphaText(Globals.TRANSLATION[16], 520, 440, Globals.FONTS['small'], Globals.COLORS['black']),
				AlphaText(Globals.TRANSLATION[17], 520, 490, Globals.FONTS['small'], Globals.COLORS['black']),
				AlphaText(Globals.TRANSLATION[35], 520, 540, Globals.FONTS['small'], Globals.COLORS['black']))
	def make_items(self):
		array = []
		for i in range(10):
			if i+1 > self.settings['volume']*10:
				temp_color = Globals.COLORS['grey31']
			else:
				temp_color = (i*self.settings['player_color'][0]/9, i*self.settings['player_color'][1]/9, i*self.settings['player_color'][2]/9)
			array.append((AlphaText(u'●', 560+i*19, 565, Globals.FONTS['symbol16'], temp_color), i))
		return (((AlphaText(Globals.LANGUAGES[self.settings['language']][1], 560, 310, self.font, Globals.COLORS['black']), 0),
				 (AlphaText(self.settings['player_name'], 560, 360, self.font, self.settings['player_color']), 1),
				 (AlphaText(Globals.TRANSLATION[self.settings['favourite_game']], 546, 410, self.font, Globals.COLORS['black']), 2),
				 (AlphaText(Globals.TRANSLATION[self.settings['music']], 560, 460, self.font, Globals.COLORS['black']), 3),
				 (AlphaText(Globals.TRANSLATION[self.settings['sound_effects']], 560, 510, self.font, Globals.COLORS['black']), 4),
				 (AlphaText('', 560, 560, self.font, Globals.COLORS['black']), 5),
				 (AlphaText(Globals.TRANSLATION[22], 'center', Globals.menu_size[1]-50, self.font, Globals.COLORS['black']), 6)),

				array)
	def init(self):
		self.settings = Globals.SETTINGS
		self.menu.init()
	def back(self, player):
		self.settings['player_name'] = player['name']
		self.settings['player_color'] = player['color']
		self.menu.init()
	def item_selected(self, item):
		MenuScreen.item_selected(self, item[0])
		if item[0] == 0:
			self.switch_language()
			self.menu.init()
		elif item[0] == 1:
			Globals.player_settings_menu_scr.init(self, Globals.SETTINGS['player_name'], Globals.SETTINGS['player_color'])
		elif item[0] == 2:
			self.settings['favourite_game'] = self.switch_game(self.settings['favourite_game'])
		elif item[0] == 3:
			self.settings['music'] = switch_music(self.settings['music'])
		elif item[0] == 4:
			self.settings['sound_effects'] = switch_sounds(self.settings['sound_effects'])
		elif item[0] == 5:
			self.change_volume(float(item[1]+1)/10)
		elif item[0] == 6:
			self.exit()
	def change_volume(self, level):
		self.settings['volume'] = level
		pygame.mixer.music.set_volume(level)
		Globals.SOUNDS['click'].set_volume(level)
	def switch_language(self):
		self.settings['language'] = int(not(self.settings['language']))
		Globals.TRANSLATION, Globals.NAMES = Globals.read_translation(self.settings['language'])
		self.static_text = self.make_static_text()
	def exit(self):
		Globals.save_settings(self.settings)
		Globals.SETTINGS = self.settings
		Globals.main_menu_scr.init()

class PlayerSettingsMenuScreen(MenuScreen):
	def __init__(self, font, itemsize):
		self.menu = CombinedMenu(self, font, itemsize)
		self.font = font
	def make_items(self):
		array = [(AlphaText(u'●', 560+i*30, 433, Globals.FONTS['symbol24'], Globals.PLAYERS_COLORS[i]), i) for i in range(len(Globals.PLAYERS_COLORS))]
		return (((AlphaText(self.player['name'], 560, 370, self.font, self.player['color']), 0),
				 (AlphaText('', 560, 430, self.font, Globals.COLORS['white']), 1),
				 (AlphaText(Globals.TRANSLATION[23], 'center', Globals.menu_size[1]-50, self.font, Globals.COLORS['black']), 2)),

				array)
	def init(self, prev_scr, name, color):
		self.static_text = [AlphaText(Globals.TRANSLATION[20], 520, 350, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[21], 520, 410, Globals.FONTS['small'], Globals.COLORS['black'])]
		if prev_scr:
			self.prev_scr = prev_scr
		self.player = {'name'	: name,
					   'color'	: color}
		self.menu.init()
	def item_selected(self, item):
		MenuScreen.item_selected(self, 0)
		if item[0] == 0:
			Globals.enter_name_menu_scr.init(self.player, item[1])
		if item[0] == 1:
			self.player['color'] = Globals.PLAYERS_COLORS[item[1]]
		elif item[0] == 2:
			self.exit()
	def exit(self):
		self.prev_scr.back(self.player)

class EnterNameMenuScreen(PlayerSettingsMenuScreen):
	def __init__(self, font, itemsize):
		self.menu = VerticalMenu(self, font, itemsize, True)
		self.font = font
	def make_items(self):
		return ((AlphaText(self.text, 560, 370, self.font, Globals.COLORS['black']), 0),
				(AlphaText('', 560, 370, self.font, Globals.COLORS['white']), 1))
	def init(self, player, item):
		self.text = player['name']
		self.color = player['color']
		self.item = item
		self.msg = {'color' : Globals.COLORS['black'],
					'num'	: 24}
		self.menu.init()
	def render(self, alpha):
		self.static_text = [AlphaText(Globals.TRANSLATION[20], 520, 350, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[21], 520, 410, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[23], 'center', Globals.menu_size[1]-50, Globals.FONTS['medium'], Globals.COLORS['black'])]
		for i in range(len(Globals.PLAYERS_COLORS)):
			self.static_text.append(AlphaText(u'●', 560+i*30, 433, Globals.FONTS['symbol24'], Globals.PLAYERS_COLORS[i]))
		PlayerSettingsMenuScreen.render(self, alpha)
		for i in range(len(Globals.PLAYERS_COLORS)):
			if i == self.item:
				pygame.draw.line(Globals.screen, Globals.COLORS['black'], (560+i*30, 467), (584+i*30, 467), 2)
		Globals.error_msg(self.msg['num'], self.msg['color'], alpha)
	def item_selected(self, item):
		MenuScreen.item_selected(self, item)
		self.exit()
	def exit(self):
		if self.text:
			Globals.player_settings_menu_scr.init(None, self.text, self.color)
		else:
			self.msg = {'color' : Globals.COLORS['red'],
						'num'	: 25}

class NewGameSettingsMenuScreen1(MenuScreen):
	def __init__(self, font, itemsize):
		self.menu = CombinedMenu(self, font, itemsize)
		self.font = font
	def make_items(self):
		array = []
		for i in range(5):
			if i < self.players_count-1:
				color = Globals.COLORS['black']
			else:
				color = Globals.COLORS['grey31']
			array.append((AlphaText(u'✪', 590+i*30, 373, Globals.FONTS['symbol24'], color), i))
		return (((AlphaText(Globals.TRANSLATION[self.game], 560, 310, self.font, Globals.COLORS['black']), 0),
				 (AlphaText('', 560, 370, self.font, Globals.COLORS['white']), 1),
				 (AlphaText(Globals.TRANSLATION[28], 560, Globals.menu_size[1]-100, self.font, Globals.COLORS['black']), 2),
				 (AlphaText(Globals.TRANSLATION[12], 527, Globals.menu_size[1]-50, self.font, Globals.COLORS['black']), 3)),

				array)
	def init(self):
		self.static_text = (AlphaText(Globals.TRANSLATION[26], 520, 290, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[27], 520, 350, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(u'✪', 560, 373, Globals.FONTS['symbol24'], Globals.COLORS['black']))
		self.game = Globals.SETTINGS['favourite_game']
		self.players_count, self.humans = read_last_game_settings()
		self.menu.init()
	def item_selected(self, item):
		MenuScreen.item_selected(self, item[0])
		if item[0] == 0:
			self.game = self.switch_game(self.game)
		elif item[0] == 1:
			self.players_count = item[1]+2
		elif item[0] == 2:
			if self.humans > self.players_count:
				self.humans = self.players_count
			Globals.new_game_settings_menu_scr_2.init(self.game, self.players_count, self.humans)
		elif item[0] == 3:
			self.exit()
	def exit(self):
		Globals.main_menu_scr.init()

class NewGameSettingsMenuScreen2(MenuScreen):
	def __init__(self, font, itemsize):
		self.font = font
		self.menu = CombinedMenu(self, font, itemsize)
	def make_items(self):
		horizontal = []
		vertical = [(AlphaText('', 560, 310, self.font, Globals.COLORS['white']), 0)]
		for i in range(self.players_count):
			if i < self.humans:
				vertical.append((AlphaText(self.players[i]['name'], 560, 370+i*30, self.font, self.players[i]['color']), i+1))
				color = Globals.COLORS['black']
			else:
				color = Globals.COLORS['grey31']
			horizontal.append((AlphaText(u'✪', 560+i*30, 313, Globals.FONTS['symbol24'], color), i))
		vertical.append((AlphaText(Globals.TRANSLATION[34], 560, Globals.menu_size[1]-100, self.font, Globals.COLORS['black']), self.humans+1))
		vertical.append((AlphaText(Globals.TRANSLATION[29], 560, Globals.menu_size[1]-50, self.font, Globals.COLORS['black']), self.humans+2))
		return (vertical, horizontal)
	def init(self, game, players_count, humans):
		self.error = False
		self.game = game
		self.players_count = players_count
		self.humans = humans
		self.players = [{'name'		: Globals.SETTINGS['player_name'],
						 'color'	: Globals.SETTINGS['player_color']}]
		self.players = self.make_comp_players(self.players)
		self.menu.init()
	def back(self, player):
		self.players[self.player_num] = player
		self.error = self.error_or_not()
		self.menu.init()
	def render(self, alpha):
		self.static_text = [AlphaText(Globals.TRANSLATION[30], 520, 290, Globals.FONTS['small'], Globals.COLORS['black']),
							AlphaText(Globals.TRANSLATION[32], 520, 350, Globals.FONTS['small'], Globals.COLORS['black'])]
		for i in range(self.humans, self.players_count):
			self.static_text.append(AlphaText(Globals.TRANSLATION[31], 560, 370+i*30, self.font, Globals.COLORS['black']))
		MenuScreen.render(self, alpha)
		if self.error:
			error_msg(33, Globals.COLORS['red'], alpha, 135)
	def item_selected(self, item):
		MenuScreen.item_selected(self, item[0])
		if item[0] == 0:
			self.humans = item[1]+1
			self.error = self.error_or_not()
		elif item[0] == self.humans+1:
			write_to_file(Globals.FILES['last_game_settings'], [str(self.players_count)+'\n', str(self.humans)+'\n'])
			self.error = self.error_or_not()
			if not self.error:
				self.players = self.make_comp_players(self.players[:self.humans])
				for i in range(len(self.players)):
					if i < self.humans:
						self.players[i]['type'] = 'human'
					else:
						self.players[i]['type'] = 'AI'
					if self.game == 5:
						self.players[i]['money'] = 1500
					else:
						self.players[i]['money'] = 20000
				Globals.game_start_animation_scr.init(self.game, self.players)
		elif item[0] == self.humans+2:
			self.exit()
		else:
			self.player_num = item[0]-1
			Globals.player_settings_menu_scr.init(self, self.players[self.player_num]['name'], self.players[self.player_num]['color'])
	def make_comp_players(self, exception):
		avail_names = list(Globals.NAMES)
		avail_colors = list(Globals.PLAYERS_COLORS)
		for i in range(self.players_count):
			if i == len(exception):
				exception.append({'name'	: random.choice(avail_names),
								  'color'	: random.choice(avail_colors)})
			if exception[i]['name'] in avail_names:
				avail_names.remove(exception[i]['name'])
			avail_colors.remove(exception[i]['color'])
		return exception
	def error_or_not(self):
		max = Globals.count_items_in_array_with_dictionaries(self.players[:self.humans])
		if max['color'] > 1 or max['name'] > 1:
			return True
		else:
			return False
	def exit(self):
		Globals.new_game_settings_menu_scr_1.init()
