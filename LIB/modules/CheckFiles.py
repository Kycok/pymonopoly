#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import Globals
from Global_Funcs import write_to_file
from os import listdir

def check_files():
	DB = listdir(Globals.DIRS['settings'])
	for FILE in ("stats", "settings", "last_game_settings"):
		if FILE not in DB:
			create_init_file(FILE)

def create_init_file(type):
	if type == 'stats':
		data = ['0\n' if x<3 else 'None 0 01.01.01 black\n' for x in range(10)]
		data = ['0\n'] + data + ['1\n'] + data
	elif type == 'settings':
		data = ('0\n', 'Player 1\n', '215\n', '0\n', '0\n', '6\n', '18\n', '18\n', '1.0\n', '1\n')
	elif type == 'last_game_settings':
		data = ("3\n", "2\n")
	write_to_file(Globals.FILES[type], data)
