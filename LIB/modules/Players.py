# -*- coding: utf-8 -*-
import Globals

class Player():
	def __init__(self, data, number):
		self.color = data['color']
		self.money = data['money']
		self.name = data['name']
		self.type = data['type']
		self.pic = Globals.FONTS['symbol16'].render(u'●', True, self.color)
		self.pos = {'field'	: 'off',
					'order'	: number}
	def render(self, offset):
		if self.pos['field'] == 'off':
			pos = (890 + self.pos['order']*20, 675 + offset)
		else:
			pos = Globals.FIELDS[str(self.pos['field'])]['pieces_pos']
		Globals.screen.blit(self.pic, pos)
