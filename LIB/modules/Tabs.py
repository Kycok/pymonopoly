# -*- coding: utf-8 -*-
import Globals
from datetime import datetime, timedelta
from InGameMenuItems import *
from pygame import draw, Rect

class TabTemplate():
	def render_menuitems(self, mp):
		for i in self.menuitems:
			if i.AV or self.cur_offset:
				if i.type in ('music_switch', 'sounds_switch', 'volume_level'):
					offset = self.cur_offset - self.volume_group_offset
				elif i.type == 'make_a_deal':
					offset = -self.cur_offset
				else:
					offset = self.cur_offset
				i.render(offset, mp)
class MainTab(TabTemplate):
	def __init__(self, init_time, timer, board, fields, players, log_messages):
		# dictionary with pictures: tuples with arguments (Sprite object, initial y-coordinate)
		self.pics = {'background'			: (Globals.PICS['background'], -125),
					 'board'				: (board, 72)}
		# dictionary with labels: lists with arguments [text, font, color, (initial coordinates), always_visible, group]
		self.labels = {'music_switch'		: [Globals.TRANSLATION[16], Globals.FONTS['small'], Globals.COLORS['grey31'], (5, -88), False, 'volume'],
					   'sounds_switch'		: [Globals.TRANSLATION[37], Globals.FONTS['small'], Globals.COLORS['grey31'], (5, -68), False, 'volume'],
					   'timer'				: [None, Globals.FONTS['big'], Globals.COLORS['grey31'], (timer.x, timer.y), True, None],
					   'user_menu'			: [AlphaText(players[0].name + Globals.TRANSLATION[48], 20, 280, Globals.FONTS['small'], Globals.COLORS['white']), None, None, (20, 280), True, 'user_menu'],
					   'volume_level'		: [Globals.TRANSLATION[36], Globals.FONTS['small'], Globals.COLORS['grey31'], (5, -108), False, 'volume']}
		# dictionary with menuitems: MenuItem objects with arguments (text, font, color, (initial coordinates), always_visible, type, size, number, Tooltip object)
		temp = 70
		if Globals.SETTINGS['language']:
			temp += 27
		self.menuitemsOPTIONS = [MenuItem(None, Globals.FONTS['symbol16'], None, (temp, -86), False, 'music_switch', (16, 16), None, None),
								 MenuItem(None, Globals.FONTS['symbol16'], None, (temp, -66), False, 'sounds_switch', (16, 16), None, None),
								 MenuItem(u'window-close.png', None, None, (Globals.menu_size[0]-37, -108), False, 'exit_main', (32, 32), None, Tooltip((40, -102), Globals.COLORS['grey31'], text=Globals.TRANSLATION[38], align='right')),
								 MenuItem(u'system-run.png', None, None, (Globals.menu_size[0]-37, -71), False, 'main_settings', (32, 32), None, Tooltip((40, -69), color=Globals.COLORS['grey31'], text=Globals.TRANSLATION[1], align='right')),
								 MenuItem(u'appointment-new.png', None, None, (Globals.menu_size[0]-37, -34), False, 'pause_game', (32, 32), None, Tooltip((40, -30), color=Globals.COLORS['grey31'], text=Globals.TRANSLATION[39], align='right'))]
		for i in range(10):
			self.menuitemsOPTIONS.append(MenuItem(u'●', Globals.FONTS['symbol11'], None, (temp+i*13, -103), False, 'volume_level', (11, 11), i, None))
		for i in range(40):
			if fields[i].group in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'):
				text, font, color, size, AIP = self.make_onboard_fields_active_zone(fields[i].group, i)
				temp = 0
				if Globals.game_start_animation_scr.game == 'manager':
					temp += 15
				self.menuitemsOPTIONS.append(MenuItem(text, font, color, Globals.FIELDS[str(i)]['group']['pos'], True, 'field', size, i, Tooltip((10, 505+temp), Globals.COLORS['grey31'], type='field', number=i), alternative_init_pos=AIP, border_size=3, border_color='black', force_border=True))
		self.menuitemsTURN = [MenuItem(Globals.TRANSLATION[46], Globals.FONTS['small'], Globals.COLORS['white'], (37, 310), True, 'roll_dices', Globals.FONTS['small'].size(Globals.TRANSLATION[46]), None, None, underline=True),
							  MenuItem(Globals.TRANSLATION[49], Globals.FONTS['small'], Globals.COLORS['white'], (37, 310), True, 'end_turn', Globals.FONTS['small'].size(Globals.TRANSLATION[49]), None, None, underline=True),
							  MenuItem(Globals.TRANSLATION[47], Globals.FONTS['small'], Globals.COLORS['white'], (37, 330), True, 'manage_company', Globals.FONTS['small'].size(Globals.TRANSLATION[47]), None, None, underline=True)]
		self.AI_or_HUMAN(players[0].type, [1])

		self.log_messages = log_messages
		self.fields = fields
		self.players = players
		if Globals.SETTINGS['music'] == 19 and Globals.SETTINGS['sound_effects'] == 19:
			self.volume_visible = False
			self.volume_group_offset = 24
		else:
			self.volume_visible = True
			self.volume_group_offset = 0
		self.visible = False
		init_time = init_time.split(':')
		self.start_time = datetime.now() - timedelta(hours=int(init_time[0]), minutes=int(init_time[1]), seconds=int(init_time[2]))
		self.cur_offset = 0
		self.max_offset = 113
		self.paused = False
		self.dices = [0, 0]
		self.TURN = 0
	def AI_or_HUMAN(self, player_type, remove_options):
		if player_type == 'AI':
			self.menuitems = self.menuitemsOPTIONS
			self.user_menu = {'show'	: False}
		else:
			self.menuitems = self.menuitemsOPTIONS + self.menuitemsTURN
			self.user_menu = {'alpha'	: 0,
							  'show'	: True}
			for i in remove_options:
				self.menuitems.remove(self.menuitemsTURN[i])
	def make_onboard_fields_active_zone(self, group, num):
		text = 'fields_pics/'
		if Globals.game_start_animation_scr.game == 'monopoly':
			text += 'colored/'
		text += group + '.png'
		font = None
		color = None
		if group in ('1', '2'):
			if Globals.game_start_animation_scr.game == 'monopoly':
				size = (47, 79)
				AIP = (-12, -27)
			else:
				size = (47, 78)
				AIP = (-12, -22)
		elif group in ('3', '4'):
			size = (77, 47)
			if Globals.game_start_animation_scr.game == 'monopoly':
				AIP = (-26, -4)
			else:
				AIP = (-30, -5)
		elif group in ('5', '6'):
			size = (47, 77)
			if Globals.game_start_animation_scr.game == 'monopoly':
				AIP = (-12, -19)
			else:
				AIP = (-12, -23)
		elif group in ('7', '8'):
			size = (77, 47)
			if Globals.game_start_animation_scr.game == 'monopoly':
				AIP = (-26, -4)
			else:
				AIP = (-24, -4)
		elif group == '9':
			if Globals.FIELDS[str(num)]['group']['text'] == u'1':
				size = (47, 79)
				AIP = (-8, -26)
			elif Globals.FIELDS[str(num)]['group']['text'] == u'3':
				size = (47, 77)
				AIP = (-8, -18)
			else:
				size = (77, 47)
				AIP = (-22, -4)
		else:
			text = ''
			font = Globals.FONTS['symbol11']
			color = Globals.COLORS['black']
			if Globals.FIELDS[str(num)]['group']['text'] == u'1':
				size = (77, 47)
				AIP = (-26, -4)
			else:
				size = (47, 77)
				AIP = (-12, -19)
		return text, font, color, size, AIP
	def upd_volume_group_pos(self):
		if self.volume_visible:
			if self.volume_group_offset:
				self.volume_group_offset -= 1
		else:
			if self.volume_group_offset < 24:
				self.volume_group_offset += 1
	def render(self, mp):
		if mp[1] <= 50+self.cur_offset:
			self.visible = True
		else:
			self.visible = False
		if self.visible and self.cur_offset < self.max_offset:
			self.cur_offset += 3
		elif not self.visible and self.cur_offset:
			self.cur_offset -= 3
		for i in self.pics.values():
			i[0].y = i[1] + self.cur_offset
		for i in ('background', 'board'):
			self.pics[i][0].render()
		draw.rect(Globals.screen, Globals.COLORS['black'], Rect((self.pics['board'][0].x, self.pics['board'][0].y), (599, 599)), 2)
		if not self.paused:
			self.labels['timer'][0] = str(datetime.now() - self.start_time).split('.')[0]
		if Globals.SETTINGS['music'] == 19 and Globals.SETTINGS['sound_effects'] == 19:
			self.volume_visible = False
		else:
			self.volume_visible = True
		self.upd_volume_group_pos()

		for i in self.labels.values():
			if i[5] == 'user_menu':
				if self.user_menu['show']:
					i[0].y = i[3][1] + self.cur_offset
					if self.user_menu['alpha'] != 255:
						self.user_menu['alpha'] += 5
					i[0].render(self.user_menu['alpha'])
			elif i[4] or self.cur_offset:
				pos = (i[3][0], i[3][1] + self.cur_offset)
				if i[5] == 'volume':
					pos = (pos[0], pos[1]-self.volume_group_offset)
				Globals.screen.blit(i[1].render(i[0], True, i[2]), pos)
		for i in range(len(self.log_messages)):
			self.log_messages[i].render(i, self.cur_offset)
		self.render_menuitems(mp)
		for i in self.fields.values():
			i.render(self.cur_offset)
		for i in self.players:
			i.render(self.cur_offset)
	def pause_game(self):
		self.paused = not self.paused
		if self.paused:
			self.pause_start_time = datetime.now()
			self.labels['timer'][2] = Globals.COLORS['red']
		else:
			self.start_time += datetime.now() - self.pause_start_time
			self.labels['timer'][2] = Globals.COLORS['grey31']
	def make_move(self):
		self.dices = [randrange(1, 7) for i in range(2)]
		if self.players[self.TURN].pos['field'] == 'off':
			self.players[self.TURN].pos['field'] = 0
		self.players[self.TURN].pos['field'] += self.dices[0] + self.dices[1]
		if self.players[self.TURN].pos['field'] > 39:
			self.players[self.TURN].pos['field'] -= 40
		self.AI_or_HUMAN(self.players[self.TURN].type, [0])
	def end_turn(self):
		if self.TURN == len(self.players)-1:
			self.TURN = 0
		else:
			self.TURN += 1
		self.labels['user_menu'][0].change_text(self.players[self.TURN].name + Globals.TRANSLATION[48])
		self.AI_or_HUMAN(self.players[self.TURN].type, [1])
class TabGroup(TabTemplate):
	def __init__(self, data):
		self.color = None
		self.show_tab = None
		self.cur_offset = 0
		self.max_offset = 200
		self.visible = False
		self.rect = Rect((Globals.menu_size[0], 165), (200, 450))
		self.tabs = [Tab(data[i], i) for i in range(len(data))]
		self.menuitems = [MenuItem(Globals.TRANSLATION[41], Globals.FONTS['micro'], Globals.COLORS['grey31'], (Globals.menu_size[0]+195-Globals.FONTS['micro'].size(Globals.TRANSLATION[41])[0], 195), False, 'make_a_deal', (Globals.FONTS['micro'].size(Globals.TRANSLATION[41])[0], 16), None, None, offset_axis='x', underline=True)]
	def render(self, mp):
		self.visible, self.show_tab = self.visible_or_not(mp)
		if self.visible or self.cur_offset:
			self.color = self.tabs[self.show_tab].color
			self.move()
			Globals.PICS['player_tab_back'].x = self.rect.x
			Globals.PICS['player_tab_back'].y = self.rect.y
			Globals.PICS['player_tab_back'].render()
			draw.rect(Globals.screen, self.color, self.rect, 2)
		self.render_menuitems(mp)
		for i in self.tabs:
			i.render(self.cur_offset, self.show_tab)
	def visible_or_not(self, mp):
		for i in self.tabs:
			active_zone_size = Globals.FONTS['small'].size(i.labels['money'][0])[0]+15
			if Rect((i.rect.x-active_zone_size, i.rect.y), (i.rect.width+active_zone_size, i.rect.height)).collidepoint(mp):
				return True, i.number
		return self.rect.collidepoint(mp), self.show_tab
	def move(self):
		if self.visible and self.cur_offset < self.max_offset:
			self.cur_offset += 5
			self.move_rect(-5, 0)
		elif not self.visible and self.cur_offset:
			self.cur_offset -= 5
			self.move_rect(5, 0)
	def move_rect(self, x, y):
		self.rect = self.rect.move(x, y)
		for i in self.tabs:
			i.rect = i.rect.move(x, y)
class Tab():
	def __init__(self, data, number):
		self.number = number
		self.color = data.color
		self.rect = Rect((Globals.menu_size[0], 165+50*number), (10, 50))
		x_name_pos = (200 - Globals.FONTS['small'].size(data.name)[0]) / 2
		x_money_size = Globals.FONTS['small'].size(str(data.money))[0]+20
		self.labels = {'money'	: [str(data.money), Globals.FONTS['small'], Globals.COLORS['grey31'], (self.rect.x-x_money_size, self.rect.y+17), True],
					   'name'	: [data.name, Globals.FONTS['small'], self.color, (Globals.menu_size[0]+x_name_pos, 175), False]}
	def render(self, offset=0, number=None):
		draw.rect(Globals.screen, self.color, self.rect, 0)
		for i in self.labels.values():
			if i[4]:
				self.render_with_offset(i, offset)
			if offset and self.number == number:
				self.render_with_offset(i, offset)
	def render_with_offset(self, data, offset):
		pos = (data[3][0] - offset, data[3][1])
		Globals.screen.blit(data[1].render(data[0], True, data[2]), pos)
