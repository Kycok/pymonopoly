# -*- coding: utf-8 -*-
import Globals, pygame
from GameGlobals import *
from GameFields import GameField
from Global_Funcs import read_ingame_translation, read_stats
from Players import *
from random import shuffle
from Sprite import *
from Tabs import TabGroup
from TransparentText import AlphaText, LogsAlphaText

class GameStartAnimationScreen():
	def init(self, game, players, init_time='0:00:00'):
		shuffle(players)
		self.players = [Player(players[i], i) for i in range(len(players))]
		self.players_info_tabs = TabGroup(self.players)
		self.players_tabs_offset = 0
		self.timeout = 100
		self.players_pics_offset = 30
		if game == 5:
			self.game = 'monopoly'
		else:
			self.game = 'manager'
		self.board = Sprite(Globals.hor_center-300, -598, Globals.BOARDS[self.game])
		self.board.y = -608
		if Globals.SETTINGS['music'] == 18:
			pygame.mixer.music.fadeout(2000)
		self.alpha = 0
		self.timer = AlphaText(init_time, 'right', 5, Globals.FONTS['big'], Globals.COLORS['grey31'], 10)
		Globals.BOARD_TEXT, Globals.LOG_MESSAGES, Globals.FIELDNAMES, Globals.RENTLABELS = read_ingame_translation(self.game, Globals.SETTINGS['language'])
		Globals.FIELDS = read_game_globals(self.game)
		self.fields = {}
		for i in range(40):
			self.fields[i] = GameField(str(i))
		self.log_messages = [LogsAlphaText(Globals.LOG_MESSAGES['gamestart'])]
		self.border_pos = 0
		temp = 0
		for i in self.players:
			if i.type == 'human':
				temp += 1
				index = self.players.index(i)
  		if temp == 1 and self.players[index].color == Globals.COLORS['green']:
			lotr = Sprite(0, 0, Globals.PICS['lotr'])
		  	Globals.PICS['background'].y = -795
			while Globals.PICS['background'].y <= -125:
				lotr.render()
				if self.timeout:
					self.timeout -= 1
				else:
					if Globals.PICS['background'].y >-197:
						Globals.PICS['background'].y += 1
					else:
						Globals.PICS['background'].y += 10
					self.render()
				Globals.window.blit(Globals.screen, (0, 0))
				pygame.display.flip()
		else:
			Globals.PICS['background'].y = -125
		self.GO = False
		self.help_rects = (pygame.Rect((0, 0), (Globals.menu_size[0], 50)),
						   pygame.Rect((Globals.menu_size[0]-70, 165), (70, 50*len(self.players))))
		while True:
			self.render()
			if self.alpha > 254:
				MO, MA = read_stats()
				if int(MO[1])+int(MA[1]) or self.GO:
					Globals.game_scr.init(init_time, self.timer, self.board, self.game, self.fields, self.players, self.players_info_tabs, self.log_messages)
				else:
					self.show_help()
			Globals.window.blit(Globals.screen, (0, 0))
			pygame.display.flip()
	def render(self):
		if self.players_tabs_offset < 10 and self.timeout%3 == 0:
			self.players_tabs_offset += 1
			for i in self.players_info_tabs.tabs:
				i.rect = i.rect.move(-1, 0)
		Globals.PICS['background'].render()
		if self.players_pics_offset:
			self.players_pics_offset -= 1
		if self.timeout:
			self.timeout -= 1
		else:
			if self.board.y == -608:
				pygame.mixer.music.load(Globals.SOUNDS['game_music'])
				if Globals.SETTINGS['music'] == 18:
					pygame.mixer.music.play(-1)
			if self.board.y < 0:
				self.board.y += 10
		if self.board.y > -1:
			if self.board.y < 72:
				self.board.y += 1
			if self.alpha < 255:
				self.alpha += 2
		self.log_messages[0].render(0, 0)
		self.timer.render(self.alpha)
		self.board.render()
		pygame.draw.rect(Globals.screen, Globals.COLORS['black'], ((self.board.x, self.board.y), (599, 599)), 2)
		for i in self.fields.keys():
			if self.fields[i].group in ('1', '2', '3', '4', '5', '6', '7', '8', '9'):
				path = 'fields_pics/'
				if Globals.game_start_animation_scr.game == 'monopoly':
					path += 'colored/'
				temp = Sprite(Globals.FIELDS[str(i)]['group']['pos'][0], Globals.FIELDS[str(i)]['group']['pos'][1]+self.board.y-72, Globals.DIRS['symbols']+path+self.fields[i].group+'.png')
				temp.render()
			self.fields[i].render(self.board.y-72)
		for i in self.players:
			i.render(self.players_pics_offset)
		for i in self.players_info_tabs.tabs:
			i.render()

	def show_help(self):
		for i in self.help_rects:
			pygame.draw.rect(Globals.screen, Globals.COLORS['red'], i, 3)
		text = Globals.TRANSLATION[45].split()
		string = ''
		y_pos = 100
		for i in range(len(text)):
			string += text[i] + ' '
			if Globals.FONTS['small'].size(string[:len(string)-1])[0] > 225:
				Globals.screen.blit(Globals.FONTS['small'].render(string[:len(string)-1], True, Globals.COLORS['grey31']), (25, y_pos))
				y_pos += 16
				string = ''
		Globals.screen.blit(Globals.FONTS['small'].render(string[:len(string)-1], True, Globals.COLORS['grey31']), (25, y_pos))
		for e in pygame.event.get():
			if e.type == pygame.QUIT:
				sys.exit()
		mp = pygame.mouse.get_pos()
		if mp[1] <= 50 or self.help_rects[1].collidepoint(mp):
			self.GO = True
