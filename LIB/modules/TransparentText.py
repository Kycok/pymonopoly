# -*- coding: utf-8 -*-
import Globals
from pygame import Surface

class ATtemplate:
	def __init__(self, x, text, font, color, x_offset=0):
		if x == 'center':
			self.x = Globals.hor_center - font.size(text)[0]/2
		elif x == 'right':
			self.x = Globals.menu_size[0] - font.size(text)[0] - x_offset
		else:
			self.x = x
		self.font = font
		self.color = color
		self.change_text(text)
	def change_text(self, text):
		self.text = self.font.render(text, True, self.color)
	def set_alpha(self, alpha):
		if alpha != 255:
			text_size = self.text.get_rect()
			img_text = Surface((text_size[2], text_size[3]))
			img_color = (255-self.color[0], 255-self.color[1], 255-self.color[2])
			img_text.fill(img_color)
			img_text.set_colorkey(img_color)
			img_text.blit(self.text, (0, 0))
			img_text.set_alpha(alpha)
			return img_text
		else:
			return self.text

class AlphaText(ATtemplate):
	def __init__(self, text, x, y, font, color, x_offset=0):
		ATtemplate.__init__(self, x, text, font, color, x_offset)
		self.y = y
		self.symbols = text
	def render(self, alpha):
		string = self.set_alpha(alpha)
		Globals.screen.blit(string, (self.x, self.y))

class Cursor(AlphaText):
	def render(self, alpha, hor, vert):
		string = self.set_alpha(alpha)
		Globals.screen.blit(string, (hor-self.x, vert-3))

class LogsAlphaText(ATtemplate):
	def __init__(self, data):
		font = Globals.FONTS['small']
		ATtemplate.__init__(self, 'center', data['text'], font, Globals.COLORS[data['color']])
		self.alpha = 1
	def render(self, number, offset):
		if self.alpha != 255:
			self.alpha += 2
		string = self.set_alpha(self.alpha)
		Globals.screen.blit(string, (self.x, 24-number*20+offset))
