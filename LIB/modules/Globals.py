# -*- coding: utf-8 -*-
import pygame
from CheckFiles import check_files
from GameScreen import *
from GameStart import *
from Global_Funcs import read_settings, read_translation
from MenuScreens import *
from Sprite import *

pygame.mixer.init()

#--- Game version, resolution, directories, files, pictures, sounds, fonts, colors and languages ----
game_version = '0.3-dev'
menu_size = (1300, 700)
hor_center = menu_size[0]/2

DIRS = {'LIB'					: 'LIB/',
		'settings'				: 'settings/'}
DIRS['images'] = DIRS['LIB'] + 'images/'
DIRS['symbols'] = DIRS['images'] + 'symbols/'
DIRS['boards'] = DIRS['images'] + 'boards/'
DIRS['sounds'] = DIRS['LIB'] + 'sound/'
DIRS['translations'] = DIRS['LIB'] + 'translations/'
DIRS['fonts'] = DIRS['LIB'] + 'fonts/'

FILES = {'last_game_settings'	: DIRS['settings'] + 'last_game_settings',
		 'settings'				: DIRS['settings'] + 'settings',
		 'stats'				: DIRS['settings'] + 'stats',
		 'regular_font'			: DIRS['fonts'] + 'Ubuntu-M.ttf',
		 'symbols_font'			: DIRS['fonts'] + 'ume-ugo5.ttf'}

BOARDS = {'manager'				: DIRS['boards'] + 'manager.jpg',
		  'monopoly'			: DIRS['boards'] + 'monopoly.jpg'}

PICS = {'left_man'				: Sprite(hor_center-425, 300, DIRS['images'] + 'menu_left_man.jpg'),
		'right_man'				: Sprite(hor_center+250, 300, DIRS['images'] + 'menu_right_man.jpg'),
		'logo'					: Sprite(hor_center-300, 0, DIRS['images'] + 'Monopoly-Logo.jpg'),
		'background'			: Sprite(0, -125, DIRS['images'] + 'background.jpg'),
		'player_tab_back'		: Sprite(0, 0, DIRS['images'] + 'player_tab_back.jpg'),
		'lotr'                  : DIRS['images'] + 'lotr.jpg'}

SOUNDS = {'click'				: pygame.mixer.Sound(DIRS['sounds'] + 'dialog-information.ogg'),
		  'game_music'			: DIRS['sounds'] + 'M3.mp3',
		  'menu_music'			: DIRS['sounds'] + 'M2.mp3'}

pygame.font.init()
FONTS = {'big'					: pygame.font.Font(FILES['regular_font'], 32),
		 'medium'				: pygame.font.Font(FILES['regular_font'], 24),
		 'small'				: pygame.font.Font(FILES['regular_font'], 16),
		 'micro'				: pygame.font.Font(FILES['regular_font'], 12),
		 'symbol24'				: pygame.font.Font(FILES['symbols_font'], 24),
		 'symbol32'				: pygame.font.Font(FILES['symbols_font'], 32),
		 'symbol16'				: pygame.font.Font(FILES['symbols_font'], 16),
		 'symbol11'				: pygame.font.Font(FILES['symbols_font'], 11)}

COLORS = {'black'				: (0, 0, 0),
		  'brown'				: (118, 48, 31),
		  'deep_blue'			: (6, 48, 180),
		  'light_blue'			: (15, 124, 164),
		  'green'				: (61, 115, 21),
		  'light_green'			: (126, 231, 85),
		  'grey31'				: (175, 175, 175),
		  'grey63'				: (95, 95, 95),
		  'magenta'				: (134, 18, 192),
		  'pink'				: (214, 17, 99),
		  'red'					: (215, 0, 0),
		  'white'				: (255, 255, 255),
		  'yellow'				: (238, 237, 17)}

PLAYERS_COLORS = (COLORS['deep_blue'],
				  COLORS['magenta'],
				  COLORS['red'],
				  COLORS['green'],
				  COLORS['pink'],
				  COLORS['light_blue'],
				  COLORS['brown'],
				  COLORS['yellow'])

LANGUAGES = (('en', u'English'),
			 ('ru', u'Русский'))

#--- Read settings and translation ------------------------------------------------------------------
check_files()
SETTINGS = read_settings()
TRANSLATION, NAMES = read_translation(SETTINGS['language'])

#--- Create main window -----------------------------------------------------------------------------
window = pygame.display.set_mode(menu_size)
pygame.display.set_caption('PyMonopoly. Version: ' + game_version)
screen = pygame.Surface(menu_size)
pygame.mixer.music.load(SOUNDS['menu_music'])
pygame.mixer.music.set_volume(SETTINGS['volume'])
SOUNDS['click'].set_volume(SETTINGS['volume'])
if SETTINGS['music'] == 18:
	pygame.mixer.music.play(-1)

#--- Create scenes ----------------------------------------------------------------------------------
main_menu_scr = MainMenuScreen(FONTS['big'], (225, 32))
stats_menu_scr = StatsMenuScreen(FONTS['medium'], (250, 32))
prefs_menu_scr = PrefsMenuScreen(FONTS['medium'], ((225, 24), (16, 16)))
player_settings_menu_scr = PlayerSettingsMenuScreen(FONTS['medium'], ((234, 24), (24, 24)))
enter_name_menu_scr = EnterNameMenuScreen(FONTS['medium'], (234, 24))
new_game_settings_menu_scr_1 = NewGameSettingsMenuScreen1(FONTS['medium'], ((185, 24), (24, 24)))
new_game_settings_menu_scr_2 = NewGameSettingsMenuScreen2(FONTS['medium'], ((174, 24), (24, 24)))

game_start_animation_scr = GameStartAnimationScreen()
game_scr = GameScreen()
