# -*- coding: utf-8 -*-
import Globals, pygame, sys
from TransparentText import Cursor

#--- Template
class Menu():
	def __init__(self, surface, font, itemsize, input=False):
		self.surface = surface
		self.itemsize = itemsize
		self.input = input
		self.cursor = {'object'		: Cursor(u'»', 20.0, 0, font, Globals.COLORS['black']),
					   'direction'	: 'right'}
	def init(self, item):
		self.separate_alphas = {}
		self.alpha = 0
		while True:
			self.items = self.surface.make_items()
			item = self.mouse_select(item)
			self.animate_cursor()
			for e in pygame.event.get():
				item = self.events(e, item)
			Globals.window.blit(Globals.screen, (0, 0))
			pygame.display.flip()
	def animate_cursor(self):
		if self.cursor['direction'] == 'right':
			if self.cursor['object'].x <= 20.0:
				self.cursor['direction'] = 'left'
			else:
				self.cursor['object'].x -= 0.1
		else:
			if self.cursor['object'].x >= 25.0:
				self.cursor['direction'] = 'right'
			else:
				self.cursor['object'].x += 0.1
	def menuitems_render(self, items, item, itemsize, orientation):
		for i in items:
			if orientation == 'vert' and i[1] in self.separate_alphas.keys():
				i[0].render(self.separate_alphas[i[1]])
			else:
				i[0].render(self.alpha)
			if item == i[1]:
				if orientation == 'vert':
					self.cursor['object'].render(self.alpha, i[0].x, i[0].y)
				else:
					pygame.draw.line(Globals.screen, Globals.COLORS['black'], (i[0].x, i[0].y+10+itemsize[1]), (i[0].x+itemsize[0], i[0].y+10+itemsize[1]), 2)
	def mouse_select(self, item):
		mp = pygame.mouse.get_pos()
		item = self.mouse_choose_item(item, mp)
		if self.alpha != 255:
			self.alpha += 5
		for i in self.separate_alphas.keys():
			if self.separate_alphas[i] != 255:
				self.separate_alphas[i] += 5
			else:
				del self.separate_alphas[i]
		self.render(item)
		return item
	def events(self, e, item):
		if e.type == pygame.QUIT:
			sys.exit()
		if e.type == pygame.KEYDOWN:
			if e.key == pygame.K_ESCAPE:
				self.surface.exit()
			if e.key in (pygame.K_RETURN, pygame.K_KP_ENTER):
				self.surface.item_selected(item)
	def vert_keypresses(self, e, items, item):
		if e.type == pygame.KEYDOWN:
			if e.key == pygame.K_UP:
				item = self.lower_item(items, item)
			if e.key == pygame.K_DOWN:
				item = self.upper_item(items, item)
		return item
	def hor_keypresses(self, e, items, item):
		if e.type == pygame.KEYDOWN:
			if e.key == pygame.K_LEFT:
				item = self.lower_item(items, item)
			if e.key == pygame.K_RIGHT:
				item = self.upper_item(items, item)
		return item
	def lower_item(self, items, item):
		if item > 0:
			item -= 1
		else:
			item = len(items)-1
		return item
	def upper_item(self, items, item):
		if item < len(items)-1:
			item += 1
		else:
			item = 0
		return item
#--- Menus
class VerticalMenu(Menu):
	def init(self):
		Menu.init(self, 0)
	def mouse_choose_item(self, item, mp):
		for i in self.items:
			if pygame.Rect((i[0].x, i[0].y), self.itemsize).collidepoint(mp):
				item = i[1]
		return item
	def render(self, item):
		self.surface.render(self.alpha)
		self.menuitems_render(self.items, item, self.itemsize, 'vert')
	def events(self, e, item):
		item = self.vert_keypresses(e, self.items, item)
		Menu.events(self, e, item)
		if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
			i = self.items[item][0]
			if pygame.Rect((i.x, i.y), self.itemsize).collidepoint(pygame.mouse.get_pos()):
				self.surface.item_selected(item)
		if self.input == True:
			if e.type == pygame.KEYDOWN:
				if e.key == pygame.K_BACKSPACE:
					self.surface.text = self.surface.text[:len(self.surface.text) - 1]
				elif e.key not in (pygame.K_RETURN, pygame.K_KP_ENTER, pygame.K_ESCAPE):
					self.surface.text += e.unicode
		return item
class CombinedMenu(Menu):
	def init(self):
		Menu.init(self, [0, 0])
	def mouse_choose_item(self, item, mp):
		for j in range(2):
			for i in self.items[j]:
				if pygame.Rect((i[0].x, i[0].y), self.itemsize[j]).collidepoint(mp):
					item[j] = i[1]
		return item
	def render(self, item):
		self.surface.render(self.alpha)
		self.menuitems_render(self.items[0], item[0], self.itemsize[0], 'vert')
		self.menuitems_render(self.items[1], item[1], self.itemsize[1], 'hor')
	def events(self, e, item):
		Menu.events(self, e, item)
		item[0] = self.vert_keypresses(e, self.items[0], item[0])
		if self.items[0][item[0]][0].symbols == '':
			item[1] = self.hor_keypresses(e, self.items[1], item[1])
		if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
			mp = pygame.mouse.get_pos()
			num = int(not self.items[0][item[0]][0].symbols)
			obj = self.items[num][item[num]][0]
			if pygame.Rect((obj.x, obj.y), self.itemsize[num]).collidepoint(mp):
				self.surface.item_selected(item)
		return item
