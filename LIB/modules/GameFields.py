# -*- coding: utf-8 -*-
import Globals

class GameField():
	def __init__(self, number):
		self.group = Globals.FIELDS[number]['group']['count']
		self.strings = {}

		# text from 'manager_board' or 'monopoly_board' file
		if number in Globals.BOARD_TEXT.keys():
			for i in range(len(Globals.BOARD_TEXT[number])):
				temp = Globals.BOARD_TEXT[number][i]
				self.strings[str(i)] = FieldObj(temp[0], temp[1][0], temp[1][1], temp[1][2])
		# field's price
		if 'price' in Globals.FIELDS[number].keys() and 'font' in Globals.FIELDS[number]['price'].keys():
			temp = Globals.FIELDS[number]['price']
			monobacs = ''
			if Globals.game_start_animation_scr.game == 'monopoly':
				monobacs += 'M'
			self.strings['price'] = FieldObj(temp['pos'], temp['count']+monobacs, temp['font'], temp['color'])
		# additional text from Globals.FIELDS variable
		if 'text' in Globals.FIELDS[number].keys():
			temp = Globals.FIELDS[number]['text']
			self.strings['text_onboard'] = FieldObj(temp['pos'], temp['count'], temp['font'], temp['color'])
		# pics of groups
		if self.group in ('1', '2', '3', '4', '5', '6', '7', '8', '9'):
			temp = Globals.FIELDS[number]['group']
			self.strings['group_num'] = FieldObj((temp['pos'][0]+temp['offset'][0], temp['pos'][1]+temp['offset'][1]), temp['text'], 'micro', 'black')

	def render(self, offset):
		for i in self.strings.values():
			Globals.screen.blit(i.pic, (i.x, i.y + offset))

class FieldObj():
	def __init__(self, pos, text, font, color):
		self.x = pos[0]
		self.y = pos[1]
		self.text = text
		self.font = Globals.FONTS[font]
		self.color = Globals.COLORS[color]
		self.pic = self.font.render(self.text, True, self.color)
