﻿# -*- coding: utf-8 -*-
from pygame import draw, mixer
import Globals
from TransparentText import AlphaText

#--- Common ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
def read_file(file):
	list = open(file, 'r')
	array = list.readlines()
	list.close()
	return map(lambda x: x.decode('UTF').strip('\n'), array)

def write_to_file(file, data, method='w'):
	list = open(file, method)
	list.writelines(map(lambda x: x.encode('UTF'), data))
	list.close()

def count_items_in_array_with_dictionaries(array):
	max = {}
	for k in array[0].keys():
		max[k] = 1
		for j in array:
			count = 0
			for i in array:
				if j[k] == i[k]:
					count += 1
			if count > max[k]:
				max[k] = count
	return max

#--- More specific --------------------------------------------------------------------------------------------------------------------------------------------------------------
def switch_music(current):
	if current == 18:
		mixer.music.fadeout(2000)
		return 19
	else:
		mixer.music.play(-1)
		return 18
def switch_sounds(current):
	if current == 18:
		current = 19
	else:
		current = 18
	Globals.SETTINGS['sound_effects'] = current
	return current
def error_msg(num, color, alpha, y_pos=150):
	x_size = Globals.FONTS['small'].size(Globals.TRANSLATION[num])[0]/2
	text = AlphaText(Globals.TRANSLATION[num], Globals.hor_center-x_size, Globals.menu_size[1]-y_pos, Globals.FONTS['small'], color)

	draw.line(Globals.screen, Globals.COLORS['black'], (Globals.hor_center-x_size, Globals.menu_size[1]-y_pos-5), (Globals.hor_center+x_size, Globals.menu_size[1]-y_pos-5), 2)
	text.render(alpha)
	draw.line(Globals.screen, Globals.COLORS['black'], (Globals.hor_center-x_size, Globals.menu_size[1]-y_pos+21), (Globals.hor_center+x_size, Globals.menu_size[1]-y_pos+21), 2)

#--- Reading and writing settings, statistics and translations ------------------------------------------------------------------------------------------------------------------
def read_settings():
	SETTINGS = read_file(Globals.FILES['settings'])
	return {'language'			: int(SETTINGS[0]),
			'player_name'		: SETTINGS[1],
			'player_color'		: (int(SETTINGS[2]), int(SETTINGS[3]), int(SETTINGS[4])),
			'favourite_game'	: int(SETTINGS[5]),
			'music'				: int(SETTINGS[6]),
			'sound_effects'		: int(SETTINGS[7]),
			'volume'			: float(SETTINGS[8]),
			'game_block'		: int(SETTINGS[9])}

def save_settings(settings):
	array = [str(settings['language']) + '\n',
			 settings['player_name'] + '\n',
			 str(settings['player_color'][0]) + '\n',
			 str(settings['player_color'][1]) + '\n',
			 str(settings['player_color'][2]) + '\n',
			 str(settings['favourite_game']) + '\n',
			 str(settings['music']) + '\n',
			 str(settings['sound_effects']) + '\n',
			 str(settings['volume']) + '\n',
			 str(settings['game_block']) + '\n']
	write_to_file(Globals.FILES['settings'], array)

def read_stats():
	array = read_file(Globals.FILES['stats'])
	for i in (4, 15):
		for j in range(0, 7):
			temp = array[i+j].split()
			array[i+j] = {'name'	: temp[0],
						  'score'	: temp[1],
						  'date'	: temp[2],
						  'color'	: temp[3]}
	# 1st: monopoly, 2nd: manager
	return array[:11], array[11:]

def read_translation(lang):
	TRANS = read_file(Globals.DIRS['translations'] + Globals.LANGUAGES[lang][0] + '/main')
	NAMES = tuple(read_file(Globals.DIRS['translations'] + Globals.LANGUAGES[lang][0] + '/names'))
	return TRANS, NAMES

def read_ingame_translation(game, lang):
	board = read_file(Globals.DIRS['translations'] + Globals.LANGUAGES[lang][0] + '/' + game + '_board')
	BOARD_TEXT = {}
	FIELDNAMES = {}
	RENTLABELS = []
	stage = 0
	for i in board:
		if i in ('---fieldnames---', '---rentlabels---'):
			stage += 1
		elif not stage:
			if i[0] == '*' and i[len(i)-1] == '*':
				field = i[1:len(i)-1]
				BOARD_TEXT[field] = []
			else:
				data = i.split()
				if len(data) == 2:
					BOARD_TEXT[field].append([(int(data[0]), int(data[1]))])
				else:
					BOARD_TEXT[field][len(BOARD_TEXT[field])-1].append(data)
		elif stage == 1:
			i = i.split(':')
			FIELDNAMES[i[0]] = i[1]
		else:
			RENTLABELS.append(i)
	log = read_file(Globals.DIRS['translations'] + Globals.LANGUAGES[lang][0] + '/log_messages')
	LOG_MESSAGES = {}
	for i in range(int(len(log)/3)):
		LOG_MESSAGES[log[i*3][3:]] = {'color'	: log[i*3+1],
									  'text'	: log[i*3+2]}
	return BOARD_TEXT, LOG_MESSAGES, FIELDNAMES, RENTLABELS

def read_last_game_settings():
	array = read_file(Globals.FILES['last_game_settings'])
	return int(array[0]), int(array[1])
