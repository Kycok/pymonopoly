# -*- coding: utf-8 -*-
import Globals, pygame, sys
from Tabs import *

class GameScreen():
	def init(self, init_time, timer, board, game, fields, players, players_tabs, log_messages):
		self.tabs = {'main'			: MainTab(init_time, timer, board, fields, players, log_messages),
					 'player_info'	: players_tabs}
		while True:
			if pygame.mouse.get_focused():
				mp = pygame.mouse.get_pos()
			else:
				mp = (0, 699)
			self.render(mp)
			for e in pygame.event.get():
				self.events(e, mp)
			Globals.window.blit(Globals.screen, (0, 0))
			pygame.display.flip()
	def render(self, mp):
		for i in sorted(self.tabs):
			self.tabs[i].render(mp)
	def events(self, e, mp):
		if e.type == pygame.QUIT:
			sys.exit()
		for tab in self.tabs.values():
			for menuitem in tab.menuitems:
				if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1 and menuitem.active_zone.collidepoint(mp) or e.type == pygame.KEYDOWN and e.key in menuitem.keys:
					if Globals.SETTINGS['sound_effects'] == 18:
						Globals.SOUNDS['click'].play()
					menuitem.action()
