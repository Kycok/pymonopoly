# -*- coding: utf-8 -*-
import Globals
from pygame import image

class Sprite:
	def __init__(self, xpos, ypos, filename):
		self.x = xpos
		self.y = ypos
		self.bitmap = image.load(filename)
	def render(self):
		Globals.screen.blit(self.bitmap, (self.x, self.y))
